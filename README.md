# Recipe App API Proxxy

NGINX proxy app for recipe app API


## Usage

### Environment variables

* 'LISTEN_PORT' - Portt to listen on (default: '8000')
* 'APP_HOST - Hostname of the app to forward requests to (default: 'app')
* 'APP_PORT' - Port of the app to forward requests to (deault: '9000')